# Background

This project is associated with a Unizin Data Platform (UDP) hack-a-thon project in February 2020. The goal of this project is to replicate a prototype of [IMS Global's LTI Insights dashboard](http://projects.imsglobal.org/lti-insights/#/).

Overall, the idea is to explore what insights about LTI tool use from event data describing LTI tool launches in the Learning Management System. Beyond using a dashboard to generate insights, this project also built machine learning models that sought to classify/predict student tool in any particular course.

![LTI Insights Dashboard](https://i.imgur.com/kBh9PbT.png)

## The script

The purpose of the script in this repository is to take a series of launch events and enrich them with metadata before leveraging them in BigQuery and DataStudio.

The launch events are sourced from Canvas in the UDP Event store. Then, based on a set of identifiers, we enrich each event with data (names, etc) and also clean up sometimes-messy identifiers for individual tools. The output of this process four different CSV files, each representing the enriched launch data in slightly different grains for use in the project.

Here is a typical tool launch event from the Canvas LMS:

```json
{
  "@context": "http://purl.imsglobal.org/ctx/caliper/v1p1",
  "id": "urn:uuid:2604610a-2f20-47d5-bbd6-31517fbc5599",
  "type": "NavigationEvent",
  "actor": {
    "id": "urn:instructure:canvas:user:120000000000012313",
    "type": "Person",
    "extensions": {
      "com.instructure.canvas": {
        "user_login": "freddie",
        "user_sis_id": "12313",
        "root_account_id": "120000000000012313",
        "root_account_lti_guid": "c78f545d267dc4eeb0760055b03e924978aac765.school.instructure.com",
        "root_account_uuid": "xeUh4Sb6srT9hSaqZzNMKx9ylLjJ3BmmTHzzmQDj",
        "entity_id": "120000000000012313"
      }
    }
  },
  "action": "NavigatedTo",
  "object": {
    "id": "urn:instructure:canvas:context_external_tool:120000000000012313",
    "type": "Entity",
    "name": "context_external_tool",
    "extensions": {
      "com.instructure.canvas": {
        "asset_name": "Kaltura: Media Gallery",
        "asset_type": "context_external_tool",
        "entity_id": "120000000000012313",
        "context_account_id": "120000000000012313",
        "http_method": "GET",
        "url": "https://bacdefg.kaf.kaltura.com/canvas/index/launch/target/course-gallery",
        "domain": "kaltura.com"
      }
    }
  },
  "eventTime": "2019-11-26T18:58:08.304Z",
  "referrer": "https://school.instructure.com/courses/123123123",
  "edApp": {
    "id": "http://school.instructure.com/",
    "type": "SoftwareApplication"
  },
  "group": {
    "id": "urn:instructure:canvas:course:120000000000012313",
    "type": "CourseOffering",
    "extensions": {
      "com.instructure.canvas": {
        "context_type": "Course",
        "entity_id": "120000000000012313"
      }
    }
  },
  "membership": {
    "id": "urn:instructure:canvas:course:120000000000012313:Learner:120000000000012313",
    "type": "Membership",
    "member": {
      "id": "urn:instructure:canvas:user:120000000000012313",
      "type": "Person"
    },
    "organization": {
      "id": "urn:instructure:canvas:course:120000000000012313",
      "type": "CourseOffering"
    },
    "roles": [
      "Learner"
    ]
  },
  "session": {
    "id": "urn:instructure:canvas:session:a2eeadba82f4d72d5ef9c509bb128be6",
    "type": "Session"
  },
  "extensions": {
    "com.instructure.canvas": {
      "hostname": "school.instructure.com",
      "request_id": "3875f320-7c09-4731-a7ce-68873eedee11",
      "user_agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) CLDB 2.0.4.04; ANGEL Secure; Chrome/69.0.3497.100; Safari/537.36",
      "client_ip": "127.0.0.1",
      "request_url": "",
      "version": "1.0.0"
    }
  }
}
```

# Generate input data

Run this query against the UDP Event store to generate the launch data that becomes input for the script that generates the enriched event data.

```sql
SELECT
  actor_id user_id,
  course_offering_id course_id,
  EXTRACT(DATE
  FROM
    DATETIME(event_time)) AS event_day,
  EXTRACT(HOUR
  FROM
    DATETIME(event_time)) AS event_hour,
  JSON_EXTRACT_SCALAR(event,
    "$[object][extensions]['com.instructure.canvas'][asset_name]") AS app_name,
  JSON_EXTRACT_SCALAR(event,
    "$[object][extensions]['com.instructure.canvas'][entity_id]") AS app_entity,
  JSON_EXTRACT_SCALAR(event,
    "$[object][extensions]['com.instructure.canvas'][url]") AS app_url
FROM
  event_store.events
WHERE
  event_time > '2019-08-15 00:00:00'
  AND event_time <= '2019-12-15 00:00:00'
  AND actor_id IS NOT NULL
  AND course_offering_id IS NOT NULL
  AND CAST(JSON_EXTRACT_SCALAR(event,
      '$.object.name') AS STRING) = 'context_external_tool'
ORDER BY
  event_time DESC
;
```

Download the results and execute `run.pl`. The script will generate four output files in the `output` directory. Create a table in BQ based for each of the four files whose names are identical to the file names generated by `run.pl`.

# Generate your feature tables

Assuming that you have created the `raw_events`, `raw_course`, `raw_person`, and `raw_person_course` tables in a BigQuery dataset, the next step is to generate features for each concept that will actually be used in our visualizations and our ML models.

## Course data

Run this query and save the output to a table called `course`.

Among other things, this will compute the z-score for the number of student launches for all tools in a course.

```sql
WITH launch_per_course_distribution AS (
  SELECT
    re.csn csn
    , COUNT(re.course_canvas_id) count_course_launches
  FROM
    ba.raw_events re
  GROUP BY
    re.csn
),
enrollments_per_course AS (
  SELECT
    rc.csn
    , SUM(rc.count_students) count_students
  FROM
    ba.raw_course rc
  GROUP BY
    csn
),
launch_per_student_per_course_distribution AS (
  SELECT
    CASE
    WHEN epc.count_students IS NULL
    THEN 0
    ELSE lpcd.count_course_launches / epc.count_students
    END  count_launch_per_student
    , lpcd.csn csn
  FROM
    launch_per_course_distribution lpcd
  LEFT JOIN
    enrollments_per_course epc ON lpcd.csn=epc.csn
),
launch_per_student_per_course_stats AS (
  SELECT
    AVG(count_launch_per_student) d_average
    , STDDEV(count_launch_per_student) d_standard_deviation
  FROM
    launch_per_student_per_course_distribution
),
course_stats AS (
  SELECT
  lpspcd.csn csn
  , lpspcd.count_launch_per_student count_launch_per_student
  , CASE
    WHEN stats.d_standard_deviation IS NOT NULL
    THEN ((lpspcd.count_launch_per_student - stats.d_average) / stats.d_standard_deviation)
    ELSE NULL
    END z_score_launch_per_student
  FROM
    launch_per_student_per_course_distribution lpspcd,
    launch_per_student_per_course_stats stats
)
SELECT
  rc.term_name term_name
  , cs.csn csn
  , rc.course_subject course_subject
  , rc.course_number course_number
  , rc.course_name course_name
  , cs.z_score_launch_per_student z_score_launch_per_student
  , SUM(rc.count_discussions) count_discussions
  , SUM(rc.count_learning_activities) count_learning_activities
  , SUM(rc.count_modules) count_modules
  , SUM(rc.count_is_sex_no_data) count_is_sex_no_data
  , SUM(rc.count_is_sex_female) count_is_sex_female
  , SUM(rc.count_is_sex_male) count_is_sex_male
  , SUM(rc.count_is_sex_none) count_is_sex_none
  , SUM(rc.count_is_sex_not_selected) count_is_sex_not_selected
  , SUM(rc.count_is_ethnicity_no_data) count_is_ethnicity_no_data
  , SUM(rc.count_is_ethnicity_american_indian) count_is_ethnicity_american_indian
  , SUM(rc.count_is_ethnicity_asian) count_is_ethnicity_asian
  , SUM(rc.count_is_ethnicity_black) count_is_ethnicity_black
  , SUM(rc.count_is_ethnicity_hispanic_or_latino) count_is_ethnicity_hispanic_or_latino
  , SUM(rc.count_is_ethnicity_native_pacific_islander) count_is_ethnicity_native_pacific_islander
  , SUM(rc.count_is_ethnicity_none) count_is_ethnicity_none
  , SUM(rc.count_is_ethnicity_not_specified) count_is_ethnicity_not_specified
  , SUM(rc.count_is_ethnicity_two_or_more_races) count_is_ethnicity_two_or_more_races
  , SUM(rc.count_is_ethnicity_white) count_is_ethnicity_white
FROM course_stats cs
INNER JOIN
  ba.raw_course rc ON cs.csn=rc.csn
GROUP BY
  rc.csn
  , cs.csn
  , rc.term_name
  , rc.course_subject
  , rc.course_number
  , rc.course_name
  , cs.z_score_launch_per_student
```

## Person data

Run this query and save the output to a table called `person`.

Among other things, this will compute the z-score for the number of student launches for all tools in a course.

```sql
WITH launch_per_person_distribution AS (
  SELECT
    count(person_canvas_id) num_person_launches
    , person_canvas_id
  FROM
    ba.raw_events
  GROUP BY
    person_canvas_id
),
launch_per_student_stats AS (
  SELECT
    AVG(num_person_launches) d_average
    , STDDEV(num_person_launches) d_standard_deviation
  FROM
    launch_per_person_distribution
)
SELECT
  rp.*
  , lppd.num_person_launches
  , CASE
    WHEN stats.d_standard_deviation IS NOT NULL
    THEN ((lppd.num_person_launches - stats.d_average) / stats.d_standard_deviation)
    ELSE NULL
    END z_score_launch_per_student
FROM
  launch_per_person_distribution lppd,
  launch_per_student_stats stats
INNER JOIN
  ba.raw_person rp ON lppd.person_canvas_id=rp.person_canvas_id
```

# Utility tables

## Person-course data

A utility table that we use merely to express the grades per enrollment for each course.

Run this query and save the output to a table called `person_course`.

```sql
SELECT
  rpc.*
FROM
  ba.raw_person_course rpc
```

## Tool launch aggregation by

Run this query and insert the output in a table called `by_person_by_course_by_tool_by_day`.

This table aggregates the raw_events data by day, resulting in a description of how many times a particular tool in a particular course on a particular day. One of the elements of this query is that it will fill in data for the days when the student did not launch a particular tool. This will enable us to generate an accurate, helpful time-series representation of launches in DataStudio.

```sql
WITH DATES AS (
  SELECT
    gen_date
  FROM
    UNNEST( GENERATE_DATE_ARRAY( DATE '2019-08-15', DATE '2019-12-15', INTERVAL 1 DAY ) ) AS gen_date
),
TOOLS AS (
  SELECT
    DISTINCT(tool_name) tool_name
  FROM
    ba.raw_events
),
AGG AS (
  SELECT
    COUNT(r.person_canvas_id) num_launches,
    r.term_name term_name,
    r.course_canvas_id course_canvas_id,
    r.person_canvas_id person_canvas_id,
    r.csn csn,
    r.event_day event_day,
    r.event_hour event_hour,
    r.day_and_hour day_and_hour,
    r.is_link is_link,
    r.is_tool is_tool,
    r.tool_name tool_name,
    CAST(r.event_day AS DATE) day
  FROM
    ba.raw_events r
  WHERE event_day != 'event_day'
  GROUP BY
    r.person_canvas_id,
    r.course_canvas_id,
    r.tool_name,
    r.event_day,
    r.event_hour,
    r.day_and_hour,
    r.term_name,
    r.csn,
    r.is_link,
    r.is_tool
)
SELECT
  IFNULL(A.num_launches,0) AS num_launches,
  A.term_name,
  A.course_canvas_id,
  A.person_canvas_id,
  A.csn,
  A.is_link,
  A.is_tool,
  A.tool_name,
  D.gen_date AS day
FROM
  DATES D
CROSS JOIN
  TOOLS T
LEFT JOIN
  AGG A
ON
  T.tool_name = A.tool_name
  AND D.gen_date=A.day
ORDER BY
  D.gen_date
```

Now let's use `by_person_by_course_by_tool_by_day` to aggregate the data over the term. Run this query and save the results to `by_person_by_course_by_tool`.

```
WITH person_data AS (
  SELECT
    p.person_canvas_id
    , p.sex
    , p.ethnicity
    , p.gpa_hs
    , p.gpa_college
    , p.num_terms_completed
    , p.num_course_completed
    , p.sum_credits_earned
    , p.z_score_launch_per_student
  FROM ba.person p
)
SELECT
  bpbcbtbd.csn
  , bpbcbtbd.person_canvas_id
  , pd.sex
  , pd.ethnicity
  , pd.gpa_hs
  , pd.gpa_college
  , pd.num_terms_completed
  , pd.num_course_completed
  , pd.sum_credits_earned
  , pd.z_score_launch_per_student
  , pc.current_score current_score
  , pc.final_score final_score
  , bpbcbtbd.tool_name
  , sum(bpbcbtbd.num_launches) num_launches
FROM ba.by_person_by_course_by_tool_by_day bpbcbtbd
LEFT JOIN person_data pd
  ON bpbcbtbd.person_canvas_id=pd.person_canvas_id
LEFT JOIN ba.person_course pc
  ON bpbcbtbd.csn=pc.csn AND bpbcbtbd.person_canvas_id=pc.person_canvas_id
GROUP BY
  bpbcbtbd.person_canvas_id
  , bpbcbtbd.csn
  , bpbcbtbd.tool_name
  , pd.sex
  , pd.ethnicity
  , pd.gpa_hs
  , pd.gpa_college
  , pd.num_terms_completed
  , pd.num_course_completed
  , pd.sum_credits_earned
  , pd.z_score_launch_per_student
  , pc.current_score
  , pc.final_score
```

# Distribution tables

Now, let's create data that helps us observe the distribution of tool launches by person in a course. We want to understand the distribution of the number of tool launches a person has in a course over the span of the term.

Run the query below and save the output into a table called `distribution_total_tool_launches_per_person_in_courses`. Note that, in the example below, we set minimum and maximum thresholds for the number of tool launches per person in a course (0 and 500, respectively), and sort actual observations of tool-launches-per_term into buckets with intervals of 20 launches.


```sql
WITH launch_distribution_stage AS (
  SELECT
    COUNT(r.person_canvas_id) launches,
    r.person_canvas_id person_canvas_id,
    r.csn csn
  FROM
    ba.raw_events r
  GROUP BY
    r.person_canvas_id,
    r.csn
),
launch_distribution_bucket AS (
  SELECT
    lds.person_canvas_id person_canvas_id
    , lds.csn csn
    , (RANGE_BUCKET(lds.launches, GENERATE_ARRAY(1,501,20)) * 20) launches
  FROM launch_distribution_stage lds
),
launch_distribution AS (
  SELECT
    ldb.csn csn
    , ldb.launches
    , COUNT(ldb.launches) launch_frequency
  FROM launch_distribution_bucket ldb
  GROUP BY
    ldb.csn
    , ldb.launches
),
base_distribution AS (
  SELECT
    a.csn csn
    , c launches
    , 0 launch_frequency
    FROM
      (SELECT DISTINCT(csn) FROM ba.course) a,
      UNNEST(GENERATE_ARRAY(0,200,2)) c
)
SELECT
  bd.csn
  , bd.launches
  , CASE
    WHEN ld.launch_frequency IS NOT NULL
    THEN ld.launch_frequency
    ELSE bd.launch_frequency
    END launch_frequency
FROM base_distribution bd
LEFT JOIN launch_distribution ld
  ON
    ld.csn=bd.csn
    AND ld.launches=bd.launches
```

--------------------------------------------------------------------------------

# DataStudio visualization

Time to open Google Data Studio!

## Create your data sources

First things first, we need to create our data sources in Data Studio. Every table you created in BigQuery should become a data source in Data Studio.

When you create the data sources, you can change the data types (if they are wrong), the labels, and whether they are available in Data Studio. So I’m going to change some of these to percentages from numbers. Some tips to keep in mind:

1. You can disable attributes that you don't intend to use
1. Verify the data types and adjust as needed

You will create a data source for these tables:

1. person
1. course
1. person-course
1. distribution_total_tool_launches_per_person_in_courses
1. by_person_by_course_by_tool_by_day
1. by_person_by_course_by_tool
1. raw_events

## Create a report
Now we’re going to create a simple report to visualize some of the data about LTI tool launches in Fall 2019 at Indiana University. Create a report and give it the title of "LTI Insights."

### Tables

Let's start with some simple tables to get a sense of the LTI tool launches in the Fall 2019 term at IU.

Add a table using `by_person_by_course_by_tool_by_day` as your data source. Remove the date-range dimension and set the primary dimension to "Courses." Then set the metric to "Launches" and make sure that you're computing the `SUM` for this metric. This table shows you how many times LTI tools were launched in a course.

Add another table using the same `by_person_by_course_by_tool_by_day` data source. Configure it the same as before except for the dimension, which you'll now set to "Tool." This table shows you how many times a particular LTI tool was launched in the Fall 2019 term.

### Text and decoration

Let's add a few text boxes and labels to give our dashboard structure and meaning to the viewer.

Add a text box to the upper-left hand of the dashboard. Input "Fall 2019 LTI Insights."

Now select the two tables you created. In the "Style" panel, deselect the "header" checkbox. Add a textbox over each table and give them the names "Launches by course" and "Launches by tool" as appropriate.

### Time series

Now let's see if we can visualize when too launches happened over the course of the academic term.

Add a time series chart. Make it's using `by_person_by_course_by_tool_by_day` as its data source. By default, the chart will computing the `SUM` of all launches from all tools for all persons by day. While useful, the visualization might be more helpful if we broke it down by tool. In the "Data" panel, set the "breakdown dimension" to "Tool." Now you can see how many times each tool was launched, each day of the Academic term.

### Score cards

To give a further sense of the data to the audience, we can add a series of score cards that simply represent the data in aggregate numbers by dimension.

Add a scorecard and use `by_person_by_course_by_tool` as the data source. Set the metric to "Launches" and the function to `SUM` – this is how many LTI launches there were in the Academic term. Add two more scores cards of the same type but set the dimensions to "Course" and "Tool" and set the function to "count distinct."

Now let's add a couple more scorecards that help us describe something about the student population. Add a scorecard and set its data source to `person`. Select the "HS GPA" metric and set the function to "AVERAGE." Repeat this for the College GPA metric.

Finally, create one last scorecard for course grade. Use the `person_course` data source and set the metric to "final score."

## Gender and race breakdown

Let's see how the LTI tool launches broke down by race and gender.

Add a pie chart to the dashboard. Set the data source to `by_person_by_course_by_tool`. Now set the dimension to "Sex" and the metric to SUM launches. In the "Filter" area of the data panel, select "Add a filter." Create a filter to exclude any value that is not Male or Female.

## Distribution of tool launch frequency in course over term

Lastly, let's visualize the distribution of number of LTI Tool launches per student, by course, throughout the term. This might help us describe, for any given course, how much LTI tools are used by the students in the course.

Add a bar chart to the dashboard. Set the data source to `distribution_total_tool_launches_per_person_in_courses`. Set the dimension to "Launches in course" and the metric to "Frequency." Make sure that the metric function is set to `SUM`.

## Styling

Let's make the dashboard looks sleek.

Set the "theme" value for the dashboard to Constellation. Now select all of the components on the dashboard and click the "Style" tab. Set the background and line colors to transparent.

Select all of the scorecards. Click the style tab. Center-align all of them.

## Filters

While nice to see the aggregate data, it'd be nice to drill into to courses, tools, and time-ranges.

Add a filter and set the data source to `by_person_by_course_by_tool`. Set the metric dimension to "Course" and its metric to "Launches" with a `SUM` function.

Add another filter with the same settings, except set the dimension to "Tool".

Now add a "date range" filter. Set the beginning and end of the date range to Aug 15, 2019 and Dec 15, 2019, respectively.

--------------------

# Training a predictive models

Let's train a model to predict (classify) the outcome of a student in the course with available data. We want to investigate whether tool use has a relationship on course outcomes. To do that, we first need to create a training set. Let's create a table in our BigQuery dataset to do that:

```
SELECT
  pct.person_canvas_id person_canvas_id
  , pct.csn csn
  , pct.tool_name tool_name
  , pct.z_score_launch_per_student z_score_launches_in_course
  , CASE
    WHEN pct.final_score >= 0 AND pct.final_score <= 100
    THEN pct.final_score
    WHEN pct.final_score < 0 THEN 0
    WHEN pct.final_score > 100 THEN 100
    WHEN pct.final_score IS NULL then NULL
    END final_score
  , p.sex
  , p.is_sex_no_data
  , p.is_sex_female
  , p.is_sex_male
  , p.is_sex_not_selected
  , p.is_sex_none
  , p.ethnicity
  , p.is_ethnicity_no_data
  , p.is_ethnicity_american_indian
  , p.is_ethnicity_asian
  , p.is_ethnicity_black
  , p.is_ethnicity_hispanic_or_latino
  , p.is_ethnicity_native_pacific_islander
  , p.is_ethnicity_not_specified
  , p.is_ethnicity_two_or_more_races
  , p.is_ethnicity_white
  , p.is_ethnicity_none
  , p.gpa_hs
  , p.gpa_college
  , p.num_terms_completed
  , p.num_course_completed
  , p.sum_credits_taken
  , p.sum_credits_earned
  , p.count_course_sections_for_all_prev_term_gteq_0_lt_4
  , p.count_course_sections_for_all_prev_term_gteq_4_lt_8
  , p.count_course_sections_for_all_prev_term_gteq_8_lt_12
  , p.count_course_sections_for_all_prev_term_gteq_12_lt_16
  , p.count_course_sections_for_all_prev_term_gteq_16_lt_20
  , p.count_course_sections_for_all_prev_term_gteq_20_lt_24
  , p.count_course_sections_for_all_prev_term_gteq_24_lt_28
  , p.count_course_sections_for_all_prev_term_gteq_32
  , p.sum_credits_taken_for_all_prev_term_gteq_0_lt_15
  , p.sum_credits_taken_for_all_prev_term_gteq_15_lt_30
  , p.sum_credits_taken_for_all_prev_term_gteq_30_lt_45
  , p.sum_credits_taken_for_all_prev_term_gteq_45_lt_60
  , p.sum_credits_taken_for_all_prev_term_gteq_60_lt_75
  , p.sum_credits_taken_for_all_prev_term_gteq_75_lt_90
  , p.sum_credits_taken_for_all_prev_term_gteq_90_lt_105
  , p.sum_credits_taken_for_all_prev_term_gteq_105_lt_120
  , p.sum_credits_taken_for_all_prev_term_gteq_120
  , p.sum_credits_earned_for_all_prev_term_gteq_0_lt_15
  , p.sum_credits_earned_for_all_prev_term_gteq_15_lt_30
  , p.sum_credits_earned_for_all_prev_term_gteq_30_lt_45
  , p.sum_credits_earned_for_all_prev_term_gteq_45_lt_60
  , p.sum_credits_earned_for_all_prev_term_gteq_60_lt_75
  , p.sum_credits_earned_for_all_prev_term_gteq_75_lt_90
  , p.sum_credits_earned_for_all_prev_term_gteq_90_lt_105
  , p.sum_credits_earned_for_all_prev_term_gteq_105_lt_120
  , p.sum_credits_earned_for_all_prev_term_gteq_120
  , p.z_score_launch_per_student z_score_launches_in_term
  , c.count_discussions
  , c.count_learning_activities
  , c.count_modules
FROM
  ba.by_person_by_course_by_tool pct
INNER JOIN
  ba.person p ON pct.person_canvas_id=p.person_canvas_id
INNER JOIN
  ba.course c ON pct.csn=c.csn
```

Here we bring together a series of demographic and learner features alongside some course features for each person-course pair in Fall 2019.

Let's make the results of this query go into `training_set`.
